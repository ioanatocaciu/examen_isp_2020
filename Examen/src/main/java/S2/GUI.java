package S2;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;

public class GUI extends JFrame {
    public static JTextField textField;
    public static JButton button;

    GUI(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
    }

    static void open() {
        try {
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader("C:\\Users\\tocac\\Desktop\\examense\\examen_isp_2020\\Examen\\src\\main\\java\\S2"));

            String l;
            l = bf.readLine();
            while (l != null) {
                String[] words = l.split(" ");
                for(String a: words){
                    StringBuilder builder=new StringBuilder(a);
                    System.out.print(builder.reverse().toString()+ " ");
                }
                textField.setText(textField.getText() +  l + "\n");
                l = bf.readLine();
            }

        } catch (Exception e) {
            System.out.println("File not found!");
        }
    }

    public void init(){
        this.setLayout(null);
        int width=150;int height = 20;

        textField = new JTextField();
        textField.setBounds(10, 50, width, height);

        button = new JButton("Write Backwards");
        button.setBounds(10, 100,width, height);
        add(textField);add(button);
    }

    public static void main(String[] args) {
        new GUI();
        open();
    }
}